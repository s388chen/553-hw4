# Name:
# PennKey:

import argparse
from collections import OrderedDict
import json
import os
import struct
import sys
import threading
from time import sleep

sys.path.append("utils")
import bmv2
import helper
from convert import *


def SendDistanceVector(router, p4info_helper, myIPs, distanceVector, dvLock):
    # TODO

    """
    Example of how to send a message to the data plane:
    dvLock.acquire()
    payload = p4info_helper.buildRoutingPayload(ip, distanceVector)
    dvLock.release()
    router.SendPacketOut(payload)
    """

    while True:
        # periodically send updates to neighbors
        sleep(10)

def RunControlPlane(router, config, p4info_helper):
    # TODO

    distanceVector = []
    dvLock = threading.Lock()
    myIPs = []

    """
    Example of how to wait for multiple digest types:
    digest_request1 = p4info_helper.buildDigestConfig("my_digest_1_t")
    digest_request2 = p4info_helper.buildDigestConfig("my_digest_2_t")
    response = router.GetDigest([digest_request1, digest_request2])
    if response.name == "my_digest_1_t":
        ...
    elif response.name == "my_digest_2_t":
        ...
    """

    """
    Example of how to parse the packed bytes of the routing update data
    data = digest.data[0].struct.members[2].bitstring
    # for each index
    prefix, length, cost = struct.unpack_from('!IBB', data, index)
    prefix = decodeIPv4(struct.pack('!I', prefix))
    """


    # Start up the update thread.  We need this in its own thread as GetDigest
    # blocks indefinitely.  Just make sure to properly lock data before access.
    update_thread = threading.Thread(target=SendDistanceVector,
                                     args=(router, p4info_helper, myIPs,
                                           distanceVector, dvLock))
    update_thread.start()

    while 1:
        # just stay running forever
        sleep(1)

    router.shutdown()

# Starts a control plane for each switch. Hardcoded for our Mininet topology.
def ConfigureNetwork(p4info_file = "build/data_plane.p4info",
                     bmv2_json = "build/data_plane.json",
                     topology_json = "configs/topology.json"):
    p4info_helper = helper.P4InfoHelper(p4info_file)
    with open(topology_json, 'r') as f:
        routers = json.load(f, object_pairs_hook=OrderedDict)['routers']

    threads = []
    port = 50051
    id_num = 0

    for name, config in routers.items():
        config = byteify(config)

        print "Connecting to P4Runtime server on {}...".format(name)
        r = bmv2.Bmv2SwitchConnection(name, "127.0.0.1:" + str(port), id_num)
        r.MasterArbitrationUpdate()
        r.SetForwardingPipelineConfig(p4info = p4info_helper.p4info,
                                      bmv2_json_file_path = bmv2_json)
        t = threading.Thread(target=RunControlPlane,
                             args=(r, config, p4info_helper))
        t.start()
        threads.append(t)

        port += 1
        id_num += 1

    for t in threads:
        t.join()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CIS553 P4Runtime Controller')

    parser.add_argument("-c", '--p4info-file',
                        help="path to P4Runtime protobuf description (text)",
                        type=str, action="store",
                        default="build/data_plane.p4info")
    parser.add_argument("-b", '--bmv2-json',
                        help="path to BMv2 switch description (json)",
                        type=str, action="store",
                        default="build/data_plane.json")
    parser.add_argument("-t", '--topology-json',
                        help="path to the topology configuration (json)",
                        type=str, action="store",
                        default="configs/topology.json")

    args = parser.parse_args()

    if not os.path.exists(args.p4info_file):
        parser.error("File %s does not exist!" % args.p4info_file)
    if not os.path.exists(args.bmv2_json):
        parser.error("File %s does not exist!" % args.bmv2_json)
    if not os.path.exists(args.topology_json):
        parser.error("File %s does not exist!" % args.topology_json)
    
    ConfigureNetwork(args.p4info_file, args.bmv2_json, args.topology_json)
